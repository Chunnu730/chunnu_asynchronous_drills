/* 
    Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.
*/
const fs = require('fs');
const boardData = require('../data/trello-callbacks/boards.json')
function callback1(id) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            let idData = boardData.find(item => id === item.id)
            if (idData) {
                resolve(idData)
            } else {
                reject("something error")
            }
        }, 2 * 1000)
    })
}

module.exports = callback1;