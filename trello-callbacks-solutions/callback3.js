const fs = require("fs")
const cardData = require('../data/trello-callbacks/cards.json');
function callback3(id) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (cardData.hasOwnProperty(id)) {
                resolve(cardData[id])
            } else {
                reject("Something wrong")
            }
        }, 2 * 1000)
    })
}

module.exports = callback3;