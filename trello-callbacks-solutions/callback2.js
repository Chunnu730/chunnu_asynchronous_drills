const fs = require('fs')
const listData = require('../data/trello-callbacks/lists.json');
function callback2(id) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (listData.hasOwnProperty(id)) {
                resolve(listData[id])
            } else {
                reject("Something wrong")
            }
        }, 2 * 1000)
    })
}

module.exports = callback2;