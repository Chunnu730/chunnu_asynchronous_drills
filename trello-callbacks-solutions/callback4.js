const callback1 = require('./callback1')
const callback2 = require('./callback2')
const callback3 = require('./callback3')
const fs = require('fs')
const boardData = require('../data/trello-callbacks/boards.json')

function callback4(Thanos) {
    return new Promise((resolve, reject) => {
        const getThonasInfo = boardData.find(item => item.name === Thanos)
        if (getThonasInfo) {
            resolve(getThonasInfo['id'])
        } else {
            reject(error)
        }
    }).then(id => {

        return callback1(id)

    }).then(data => {

        return callback2(data.id)

    }).then(data => {

        const getMindInfo = data.find(item => item.name === 'Mind')

        return callback3(getMindInfo['id'])

    })

}

module.exports = callback4;