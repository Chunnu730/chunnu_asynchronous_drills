const problem2 = require('../problem2');

async function test() {
    try {
        const result1 = await problem2.function1();
        console.log(result1);

        const result2 = await problem2.function2();
        console.log(result2);

        const result3 = await problem2.function3();
        console.log(result3);

        const result4 = await problem2.function4();
        console.log(result4);

        const result5 = await problem2.function5();
        console.log(result5);
    } catch (error) {
        console.log(error);
    }
}

test();