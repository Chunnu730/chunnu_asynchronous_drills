const problem1 = require('../problem1.js');
const fs = require('fs');

async function test() {
    try {
        const result1 = await problem1.createFolder();
        console.log(result1);

        const result2 = await problem1.createFile(19);
        console.log(result2);

        const result3 = await problem1.deleteFiles();
        console.log(result3);
    } catch (error) {
        console.log(error);
    }
}

test();