const fs = require('fs')

function createFolder() {
    return new Promise((resolve, reject) => {
        fs.mkdir('./fs-callbacks-solutions/folder', (error) => {
           if(error){
               reject(error)
           }else{
               resolve("Created a folder")
           }
        })
    })
}

function createFile(num){
    return new Promise((resolve, reject)=>{
        const content = 'This is my random json file'
        for (let i = 1; i <= num; i++) {
            fs.writeFile('./fs-callbacks-solutions/folder/first' + i + '.json', content, (error) => {
                if (error) {
                    reject("error")
                } else {
                    resolve(`created ${num} random json files in folder`)
                }
            })
        }
    })
}

function deleteFiles() {
    return new Promise((resolve, reject) => {
        fs.rm('./fs-callbacks-solutions/folder/', { recursive: true }, (error) => {
            if (error) {
                reject(error)
            } else {
                resolve("successfully deleted")
            }
        })
    })
}

module.exports = { createFolder, createFile, deleteFiles }